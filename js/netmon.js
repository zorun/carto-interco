
var map;
var tenantList = [];
var providerList = [];

function init() {
	map = L.map('map', { zoomControl:true }).setView([47.078, 3.812], 6);

	// #6/46.823/6.669

	if (L.Browser.mobile) {
		map.removeControl(map.zoomControl);
	}

	var mapnikUrl = 'http://b.www.toolserver.org/tiles/bw-mapnik/{z}/{x}/{y}.png';
	var vmfabsUrl = 'https://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png';
	var osmUrl='https://a.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var ignsatUrl='https://wxs.ign.fr/3tz7turb776yzy17kwux3nkf/geoportail/wmts?layer=ORTHOIMAGERY.ORTHOPHOTOS&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/jpeg&TileMatrix={z}&TileCol={x}&TileRow={y}';
	var ignUrl='https://wxs.ign.fr/3tz7turb776yzy17kwux3nkf/geoportail/wmts?layer=GEOGRAPHICALGRIDSYSTEMS.MAPS&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/jpeg&TileMatrix={z}&TileCol={x}&TileRow={y}';
	var minimal = L.tileLayer(mapnikUrl, { maxZoom: 18 });
	var vmfabs = L.tileLayer(vmfabsUrl,  { maxZoom: 18 });
	var osm = L.tileLayer(osmUrl, { maxZoom: 18 });
	var ignsat = L.tileLayer(ignsatUrl, { maxZoom: 18 });
	var ign = L.tileLayer(ignUrl, { maxZoom: 18 });

	var baseMaps = { 
		'HillShade': vmfabs,
		'OSM': osm,
		'IGN': ign,
		'Minimal': minimal,
		'Satellite': ignsat,
	};

	facilities=new L.layerGroup();
	network=new L.layerGroup();

	var overlayMaps = {
		'Locations': facilities,
		'Network': network,
	}       
	L.control.layers(baseMaps, overlayMaps, { autoZIndex: true }).addTo(map);

	osm.addTo(map);

	load_geojson(tenantList, providerList);

	$('#sidebar').hide();
	$("#sidebar_button").click(function() {
		$("#sidebar").toggle();
	}).tooltip({
		placement : 'left',
		title : 'Afficher/masquer la sidebar'
	});

	var first_coord = [0, 0];
	var second_coord = [0, 0];
	var string_coord = '';
	$("#gpspos").html('C1 : ' + first_coord[0] + " / " + first_coord[1] + '<br/>C2 : ' + second_coord[0] + " / " + second_coord[1]);

	map.on('click', function(e) {
		if (first_coord[0] == 0) {
			first_coord = [ e.latlng.lat.toString().slice(0,9), e.latlng.lng.toString().slice(0,8) ];
			string_coord = 'GPSPOS : '+ first_coord[0] + " / " + first_coord[1] + '<br/><a href=# onClick=pop_nearby_links("'+first_coord[0]+'","'+first_coord[1]+'")>Spectre hertzien à proximité</a>';
		} else if (second_coord[0] == 0) {
			second_coord = [ e.latlng.lat.toString().slice(0,9), e.latlng.lng.toString().slice(0,8) ];
			string_coord = 'C1 : ' + first_coord[0] + " / " + first_coord[1] + '<br/>C2 : ' + second_coord[0] + " / " + second_coord[1] + '<br/><a href=# onClick=pop_heywhatsthat("'+first_coord[0]+','+first_coord[1]+',,6","'+second_coord[0]+','+second_coord[1]+',,6")>Vue &eacute;levation</a>';
		} else {1
			first_coord = [0, 0];
			second_coord = [0, 0];
			string_coord = '';
		}
		$("#gpspos").html(string_coord);
	});

	var hash = new L.Hash(map);
}

function load_geojson (tenantList, providerList) {
	var geojsonMarkerOptions = {
		radius: 6,
		color: "#000",
		fill: 1,
		weight: 1,
		opacity: 1,
		zindex: 1,
		fillOpacity: 1,
		draggable: true
	};

	var geojsonPolyLineOptions = {
		weight: 3,
		fill: 1,
		zindex: 2,
		opacity: 0.8,
	};

	var okProdMarker = L.icon({
		iconUrl: 'images/green.png',
		iconAnchor: [ 8, 8 ],
	});

	var nokProdMarker = L.icon({
		iconUrl: 'images/red.png',
		iconAnchor: [ 8, 8 ],
	});

	var worktodoProdMarker = L.icon({
		iconUrl: 'images/worker_sign.png',
		iconAnchor: [ 10, 10 ],
	});

	var waitingMarker = L.icon({
		iconUrl: 'images/yellow.png',
		iconAnchor: [ 8, 8 ],
	});

	var terminatedMarker = L.icon({
		iconUrl: 'images/blue.png',
		inconAnchor: [ 8, 8 ],
	});

	var eligibleMarker = L.icon({
		iconUrl: 'images/purple.png',
		iconAnchor: [ 8, 8 ],
	});

	// On load le réseau
	$.get("https://netbox.polyno.me/circuits/circuits/geojson/", function(data) {
		var geojsonLayer = new L.GeoJSON(data, {
			style: function(feature) {
				if (feature.geometry.type == 'LineString') {
					geojsonPolyLineOptions.color = '#009900';
				//	feature.properties.color;
					return geojsonPolyLineOptions;
				}
			},
			onEachFeature: function(feature, layer) {
				if (feature.geometry.type == 'LineString') {
					layer.on({
						click: function(e) { 
							createPopup(e.target.feature, e.target);
							//      e.target.openPopup();
						}
					});
				}
			}
		} );

		network.clearLayers();
		network.addLayer(geojsonLayer);
		network.addTo(map);
		map.almostOver.addLayer(network);
		map.on('almost:click', function (e) { e.layer.fire('click', e); });
	});

        $.get("https://netbox.polyno.me/dcim/facilities/geojson/", function(data) {
                var geojsonLayer = new L.GeoJSON(data, {
                        style: function(feature) {
                                return {fillColor: '#009900'};
                        },
                        pointToLayer: function(feature, latlng) {
                                geojsonMarkerOptions.icon=okProdMarker;
                                return L.marker(latlng, geojsonMarkerOptions);
                        },
                        onEachFeature: function(feature, layer) {
                                createPopup(feature, layer);
                        }
                } );
                facilities.clearLayers();
                facilities.addLayer(geojsonLayer);
                facilities.addTo(map);
        });



}

function createPopup (feature, layer) {
	console.log(feature.geometry.type);
	if (feature.geometry.type == 'Point') {
		var divNode = document.createElement('DIV');
                divNode.setAttribute('style', 'width:450px');
		divNode.innerHTML = '<h4 align="center">'+feature.properties.name+' ('+feature.properties.description+')</h4>';
		for (var i in feature.properties.sites) {
			var site = feature.properties.sites[i];
			divNode.innerHTML = divNode.innerHTML + site.tenant+' (AS'+site.asn+')<br/>';
		}
		console.log(divNode.innerHTML);
		return layer.bindPopup(divNode, { maxWidth:450 });
	} else if (feature.geometry.type == 'LineString') {
                var divNode = document.createElement('DIV');
                divNode.setAttribute('style', 'width:450px');
                divNode.innerHTML = '<h4 align="center">'+feature.properties.link_type+' '+feature.properties.provider+' ('+feature.properties.tenant+')</h4>';
		divNode.innerHTML = divNode.innerHTML + 'Commit : '+(feature.properties.commit/1000)+'Mbps <br/>Ports speed : '+(feature.properties.port_speed_a/1000)+'Mbps / '+(feature.properties.port_speed_b/1000)+'Mbps';
		console.log(divNode.innerHTML);
                return layer.bindPopup(divNode, { maxWidth:450 });
	}
}

Number.prototype.toRad = function() {
	return this * Math.PI / 180;
}

Number.prototype.toDeg = function() {
	return this * 180 / Math.PI;
}
